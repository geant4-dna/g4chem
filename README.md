# README #

Testing tree for Geant4-DNA chemistry

### What is this repository for? ###

This repository is intended to be used with a Geant4-DNA git submodule.
It has been created for testing alpha developments of the chemistry and geometry codes.
The backbone code corresponds to the Geant4 10.03 release version - with minor modifications:

* do not build hadronic processes
* minor modification in the geometry package (will correspond to 10.03 patch 1)
* Geant4-DNA is a git submodule, it includes alpha version of the chemistry code

### How do I get set up? ###


```
#!tcsh

git clone git@bitbucket.org:matkara/g4chem.git

cd g4chem

```

HERE ADD THE G4DNA SUBMODULE: private for now

```
#!tcsh

cd ..

mkdir g4chem-build

cd g4chem-build

cmake ../g4chem

```