git submodule init
git remote set-url origin git@$USERNAME:$PASSWORD@bitbucket.org/matkara/g4chem.git
git config --file=.gitmodules submodule.dna.url git@$USERNAME:$PASSWORD@bitbucket.org/matkara/dnachem.git
git submodule update --init
g4dir=`pwd`
cd ..
mkdir g4dir-build && cd g4dir-build
cmake $g4dir
make
